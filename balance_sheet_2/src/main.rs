
/*
	cargo run
*/

use std::io;

fn main () {
    println! ("prompt:");

    let mut GUESS = String::new ();

	io::stdin ()
	.read_line (& mut GUESS)
	.expect ("failure.");

    println! ("'{GUESS}' was inputted.");
}